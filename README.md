# battleship

Get a starting grid and exchange coordinated on you RYA Training charts for a friendly game of battleship chart practice.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# About the project

This is a game to practice latitude and longitude as a battleship game on the RYA Training maps you have for working through your various RYA training qualifications.

No affiliation to the RYA or whatever course provider you may have got the charts from.

## How-to

1. Sign up
1. Start a game, you will get a map area, start time and vessel information to place (size & draft)
1. Send the game to friends you want to join so you can all play together.
1. Place your boats by giving their lat+long and bearing. Make sure you check the tide doesn't ground your ships straight away!
1. Once all players have placed boats, start launching shells into the map by providing target latitude and longitude.

You plot your ships and all shots taken/received. You will have to answer the other players if they hit your own boat.

Remember to account for the current tidal rise at the game time and perhaps think about players ship drafts to narrow down where they might be.

## What the game does

The game will provide a few things for you to make this work;

- Each player gets a unique symbol you can mark next to their strikes as you plot them if you want to keep track.
- The game holds all players vessels and shots to validate correct answers. If you incorrectly state a miss or hit then one of your vessels will instantly sink as you seem unable to even navigate away from the coastline.
- The game will cycle around each player so you take turns to choose a target.
- Each shell fired has a splash radius of a certain size.

## Preview

![Mockup UI](docs/game_mock.png)

# Development

Version 1.0 would be all of the above completed.

## Plans for V0.1

- Create game ID at unique URL
- Game page to display the play area coordinates.
- Players list to be added/removed from.
- Adding a player shows their number of ships to place, requires coordinate & bearing input.

## Plans for V0.2

- Turn based gameplay for accepting target co-ordinates.
- Validation of all entered coordinates with map area and vessel locations.
- UI to guide players though what their supposed to do

## Plans for V0.3 - Playable maybe?

- User signup such that only 1 player can join as themselves
- Starting a game can only be done by a user
- Invite links would be unique. (unique game ID's, any signed in user can join before the start.)

## Plans for V0.4

- Add a start time to created games
- Factor tide level into coordinate validation
